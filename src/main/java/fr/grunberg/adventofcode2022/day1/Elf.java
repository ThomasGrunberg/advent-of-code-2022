package fr.grunberg.adventofcode2022.day1;

public class Elf {
	private final int caloriesCarried;
	
	public Elf(int caloriesCarried) {
		this.caloriesCarried = caloriesCarried;
	}
	
	public int getCaloriesCarried() {
		return caloriesCarried;
	}
}
