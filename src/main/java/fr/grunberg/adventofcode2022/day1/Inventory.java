package fr.grunberg.adventofcode2022.day1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.SequencedCollection;

public class Inventory {
	private Collection<Elf> elves;
	
	public Inventory(SequencedCollection<String> register) {
		this.elves = new ArrayList<>();
		int currentElfRegister = 0;
		for(String registerLine : register) {
			try {
				currentElfRegister += Integer.parseInt(registerLine);
			}
			catch(NumberFormatException e) {
				if(currentElfRegister > 0) {
					createElf(currentElfRegister);
					currentElfRegister = 0;
				}
			}
		}
		if(currentElfRegister > 0)
			createElf(currentElfRegister);
	}
	
	private void createElf(int elfRegister) {
		elves.add(new Elf(elfRegister));
	}
	
	public int getMaxCalories() {
		return elves.stream().mapToInt(Elf::getCaloriesCarried).max().getAsInt();
	}
	
	public int getThreeMaxCalories() {
		return elves.stream()
				.map(elf -> (Integer) elf.getCaloriesCarried())
				.sorted(Comparator.reverseOrder())
				.limit(3)
				.mapToInt(Integer::intValue)
				.sum();
	}
}
