package fr.grunberg.adventofcode2022.day10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SequencedCollection;

public class CPU {
	private Map<Integer, Integer> executionHistory;
	private SequencedCollection<String> screen;
	private static final Collection<Integer> signalStrengthSteps = List.of(20, 60, 100, 140, 180, 220);

	public CPU(SequencedCollection<String> instructions) {
		process(instructions);
	}

	private void process(SequencedCollection<String> instructions) {
		executionHistory = new HashMap<>();
		screen = new ArrayList<>();
		screen.add("");
		int registerX = 1;
		int cycle = 1;
		executionHistory.put(cycle, registerX);
		for(String instruction : instructions) {
			if("noop".equals(instruction)) {
				cycle++;
				screen = calculateNextPixel(registerX, screen);
				executionHistory.put(cycle, registerX);
			}
			else {
				String[] subinstructions = instruction.split(" ");
				int registerIncrement = Integer.parseInt(subinstructions[1]);
				cycle++;
				screen = calculateNextPixel(registerX, screen);
				executionHistory.put(cycle, registerX);
				cycle++;
				screen = calculateNextPixel(registerX, screen);
				registerX += registerIncrement;
				executionHistory.put(cycle, registerX);
			}
		}
	}
	
	private SequencedCollection<String> calculateNextPixel(int registerX, SequencedCollection<String> screen) {
		String currentScreenLine = screen.getLast();
		if(currentScreenLine.length() >= 40) {
			currentScreenLine = "";
			screen.add(currentScreenLine);
		}
		if(registerX == currentScreenLine.length()
				|| registerX == currentScreenLine.length()-1
				|| registerX == currentScreenLine.length()+1
				)
			currentScreenLine = currentScreenLine + "#";
		else
			currentScreenLine = currentScreenLine + ".";
		screen.removeLast();
		screen.add(currentScreenLine);
		return screen;
	}
	
	public int calculateSignalStrength() {
		return signalStrengthSteps.stream()
				.mapToInt(s -> s * executionHistory.get(s))
				.sum();
	}

	public SequencedCollection<String> renderScreen() {
		return screen;
	}
}
