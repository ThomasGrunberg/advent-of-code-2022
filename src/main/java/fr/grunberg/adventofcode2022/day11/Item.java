package fr.grunberg.adventofcode2022.day11;

public class Item {
	private final int initialWorry;
	private long worry;
	
	Item(int worry) {
		this.initialWorry = worry;
		this.worry = worry;
	}
	
	public long getWorry() {
		return worry;
	}
	public void setWorry(long worry) {
		this.worry = worry;
	}
	public int getInitialWorry() {
		return initialWorry;
	}
}
