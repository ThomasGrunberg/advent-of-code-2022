package fr.grunberg.adventofcode2022.day11;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Optional;
import java.util.SequencedCollection;
import java.util.function.Function;

public class Monkey {
	private final int number;
	private final SequencedCollection<Item> heldItems;
	private final Function<Long, Long> worryImpact;
	private final int divisibleTest;
	private final int monkeyNumberIfTestTrue;
	private final int monkeyNumberIfTestFalse;
	private final boolean divideWorry;
	
	private Monkey monkeyIfTestTrue;
	private Monkey monkeyIfTestFalse;
	private long moduloSpecial;
	
	private long numberOfItemsExamined;
	
	Monkey(int number, SequencedCollection<Item> heldItems, 
			Function<Long, Long> worryImpact, int divisibleTest,
			int monkeyNumberIfTestTrue, int monkeyNumberIfTestFalse, 
			boolean divideWorry) {
		this.number = number;
		this.heldItems = new LinkedList<>();
		this.worryImpact = worryImpact;
		this.divisibleTest = divisibleTest;
		this.monkeyNumberIfTestTrue = monkeyNumberIfTestTrue;
		this.monkeyNumberIfTestFalse = monkeyNumberIfTestFalse;
		this.divideWorry = divideWorry;
		
		this.heldItems.addAll(heldItems);
		numberOfItemsExamined = 0;
	}
	
	void tossItem(Item item) {
		heldItems.addLast(item);
	}
	void examineItems() {
		Iterator<Item> itemsIterator = heldItems.iterator();
		while(itemsIterator.hasNext()) {
			Item item = itemsIterator.next();
			item.setWorry(worryImpact.apply(item.getWorry()));
			if(divideWorry)
				item.setWorry(item.getWorry() / 3);
			else
				item.setWorry(item.getWorry() % moduloSpecial);
			boolean test = (item.getWorry() % divisibleTest) == 0;
			if(test)
				monkeyIfTestTrue.tossItem(item);
			else
				monkeyIfTestFalse.tossItem(item);
			numberOfItemsExamined++;
			itemsIterator.remove();
		}
	}
	
	int getNumber() {
		return number;
	}
	int getMonkeyNumberIfTestTrue() {
		return monkeyNumberIfTestTrue;
	}
	int getMonkeyNumberIfTestFalse() {
		return monkeyNumberIfTestFalse;
	}
	public long getNumberOfItemsExamined() {
		return numberOfItemsExamined;
	}
	long getDivisibleTest() {
		return divisibleTest;
	}
	void assignMonkeys(Collection<Monkey> monkeys) {
		Optional<Monkey> monkeyIfTrue = monkeys.stream()
				.filter(m -> m.getNumber() == monkeyNumberIfTestTrue)
				.findAny();
		if(monkeyIfTrue.isEmpty())
			throw new RuntimeException("Can't find monkey " + monkeyNumberIfTestTrue);
		Optional<Monkey> monkeyIfFalse = monkeys.stream()
				.filter(m -> m.getNumber() == monkeyNumberIfTestFalse)
				.findAny();
		if(monkeyIfFalse.isEmpty())
			throw new RuntimeException("Can't find monkey " + monkeyNumberIfTestFalse);
		monkeyIfTestTrue  = monkeyIfTrue.get();
		monkeyIfTestFalse = monkeyIfFalse.get();
	}

	@Override
	public String toString() {
		return "Monkey " + number;
	}

	void setModuloSpecial(long moduloSpecial) {
		this.moduloSpecial = moduloSpecial;
	}
}
