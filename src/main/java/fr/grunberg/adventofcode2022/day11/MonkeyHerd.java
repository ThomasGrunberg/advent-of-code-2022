package fr.grunberg.adventofcode2022.day11;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SequencedCollection;
import java.util.function.Function;

public class MonkeyHerd {
	private final Collection<Monkey> monkeys;

	public MonkeyHerd(SequencedCollection<String> monkeysDescription, boolean divideWorry, int numberOfRounds) {
		this.monkeys = parseMonkeys(monkeysDescription, divideWorry);
		for(Monkey monkey : monkeys)
			monkey.assignMonkeys(monkeys);
		long moduloSpecial = 1;
		for(Monkey monkey : monkeys)
			moduloSpecial *= monkey.getDivisibleTest();
		for(Monkey monkey : monkeys)
			monkey.setModuloSpecial(moduloSpecial);
		tossItems(numberOfRounds);
	}
	
	private void tossItems(int tossRounds) {
		for(int r = 1; r <= tossRounds; r++) {
			for(Monkey monkey : monkeys)
				monkey.examineItems();
		}
	}
	
	private Collection<Monkey> parseMonkeys(SequencedCollection<String> monkeysDescription, boolean divideWorry) {
		Collection<Monkey> monkeys = new LinkedList<>();
		Iterator<String> descriptionLines = monkeysDescription.iterator();
		List<String> monkeyDescription = new LinkedList<>();
		while(descriptionLines.hasNext()) {
			String descriptionLine = descriptionLines.next();
			if(descriptionLine.isBlank()) {
				monkeys.add(parseMonkey(monkeyDescription, divideWorry));
				monkeyDescription.clear();
			}
			else {
				monkeyDescription.add(descriptionLine);
			}
		}
		if(!monkeyDescription.isEmpty())
			monkeys.add(parseMonkey(monkeyDescription, divideWorry));
		return monkeys;
	}

	private Monkey parseMonkey(List<String> monkeyDescription, boolean divideWorry) {
		int monkeyNumber = Integer.parseInt(monkeyDescription.get(0).trim()
				.replace("Monkey ", "")
				.replace(":", "")
				);
		SequencedCollection<String> startingItemsWorry = List.of(monkeyDescription.get(1).trim()
				.replace("Starting items: ", "")
				.replace(" ", "")
				.split(",")
				);
		SequencedCollection<Item> startingItems = startingItemsWorry.stream()
				.map(Integer::parseInt)
				.map(w -> new Item(w))
				.toList();
		String[] worryImpactDetails = monkeyDescription.get(2).trim()
				.replace("Operation: ", "")
				.split(" ");
		Function<Long, Long> worryImpact;
		if("old".equals(worryImpactDetails[2])
				&& "+".equals(worryImpactDetails[3]) 
				&& "old".equals(worryImpactDetails[4]) 
				) {
			worryImpact = (Long previousImpact) -> previousImpact*2;
		}
		else if("old".equals(worryImpactDetails[2])
				&& "*".equals(worryImpactDetails[3]) 
				&& "old".equals(worryImpactDetails[4]) 
				) {
			worryImpact = (Long previousImpact) -> previousImpact * previousImpact;
		}
		else if("old".equals(worryImpactDetails[2])
				&& "+".equals(worryImpactDetails[3]) 
				) {
			int increment = Integer.parseInt(worryImpactDetails[4]);
			worryImpact = (Long previousImpact) -> previousImpact + increment;
		}
		else if("old".equals(worryImpactDetails[2])
				&& "*".equals(worryImpactDetails[3]) 
				) {
			int factor = Integer.parseInt(worryImpactDetails[4]);
			worryImpact = (Long previousImpact) -> previousImpact * factor;
		}
		else {
			throw new RuntimeException("Unknown operation : " + monkeyDescription.get(2));
		}
		int divisibleTest = Integer.parseInt(monkeyDescription.get(3).trim()
				.replace("Test: divisible by ", ""));
		int monkeyNumberIfTestTrue = Integer.parseInt(monkeyDescription.get(4).trim()
				.replace("If true: throw to monkey ", ""));
		int monkeyNumberIfTestFalse = Integer.parseInt(monkeyDescription.get(5).trim()
				.replace("If false: throw to monkey ", ""));
		return new Monkey(monkeyNumber, startingItems, worryImpact, 
				divisibleTest, monkeyNumberIfTestTrue, monkeyNumberIfTestFalse,
				divideWorry
				);
	}

	public long getMonkeyBusiness() {
		List<Long> itemsExaminedPerMonkeys = monkeys.stream()
				.map(m -> (Long) m.getNumberOfItemsExamined())
				.sorted(Collections.reverseOrder())
				.toList();
		return itemsExaminedPerMonkeys.get(0) * itemsExaminedPerMonkeys.get(1);
	}
}
