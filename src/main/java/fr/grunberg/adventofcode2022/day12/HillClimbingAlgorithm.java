package fr.grunberg.adventofcode2022.day12;

import java.util.SequencedCollection;

public class HillClimbingAlgorithm {
	private final Map map;

	public HillClimbingAlgorithm(SequencedCollection<String> mapDescription) {
		map = new Map(mapDescription);
	}

	public SequencedCollection<MapPosition> getShortestPath() {
		return map.findShortestPath();
	}
	
	public SequencedCollection<MapPosition> getShortestPathToEdge() {
		return map.findShortestPathToEdge();
	}
}
