package fr.grunberg.adventofcode2022.day12;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SequencedCollection;

class Map {
	private final List<List<MapPosition>> mapPositions;
	private MapPosition start;
	private MapPosition end;
	
	Map(SequencedCollection<String> mapDescription) {
		mapPositions = new ArrayList<>(mapDescription.size());
		int y = 0;
		for(String mapLine : mapDescription) {
			int x = 0;
			List<MapPosition> mapPositionsLine = new ArrayList<>();
			for(String mapSpot : mapLine.split("")) {
				int mapSpotHeight;
				if("S".equals(mapSpot))
					mapSpotHeight = 0;
				else if("E".equals(mapSpot))
					mapSpotHeight = 26;
				else
					mapSpotHeight = mapSpot.codePointAt(0) - "a".codePointAt(0);
				MapPosition mapPosition = new MapPosition(x, y, mapSpotHeight, mapSpot);
				mapPositionsLine.add(mapPosition);
				if("S".equals(mapSpot))
					start = mapPosition;
				else if("E".equals(mapSpot))
					end = mapPosition;
				x++;
			}
			mapPositions.add(mapPositionsLine);
			y++;
		}
		for(y = 0; y < mapPositions.size(); y++) {
			for(int x = 0; x < mapPositions.get(y).size(); x++) {
				MapPosition mapPosition = mapPositions.get(y).get(x);
				MapPosition north = (y > 0 ? mapPositions.get(y-1).get(x) : null);
				MapPosition south = (y < mapPositions.size()-1 ? mapPositions.get(y+1).get(x) : null);
				MapPosition east = (x < mapPositions.get(y).size()-1 ? mapPositions.get(y).get(x+1) : null);
				MapPosition west = (x > 0 ? mapPositions.get(y).get(x-1) : null);
				mapPosition.setNeighboors(north, south, east, west);
			}
		}
	}

	public SequencedCollection<MapPosition> findShortestPath() {
		end.checkShortestPath();
		return start.getShortestPath();
	}
	public SequencedCollection<MapPosition> findShortestPathToEdge() {
		end.checkShortestPath();
		Collection<MapPosition> edges = mapPositions.stream()
				.flatMap(m -> m.stream())
				.filter(p -> p.getX() == 0 
					|| p.getX() == mapPositions.get(0).size()
					|| p.getY() == 0
					|| p.getY() == mapPositions.size()
					)
				.filter(p -> p.getHeight() == 0)
				.filter(p -> p.getShortestPath() != null)
				.toList();
		return edges.stream()
				.sorted((p1, p2) -> p1.getShortestPath().size() - p2.getShortestPath().size())
				.toList()
				.get(0)
				.getShortestPath();
	}
}
