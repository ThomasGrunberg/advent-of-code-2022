package fr.grunberg.adventofcode2022.day12;

import java.util.LinkedList;
import java.util.List;
import java.util.SequencedCollection;
import java.util.stream.Collectors;

class MapPosition {
	private final int x;
	private final int y;
	private final int height;
	private final String character;

	private SequencedCollection<MapPosition> shortestPath;
	private MapPosition north;
	private MapPosition south;
	private MapPosition east;
	private MapPosition west;
	
	MapPosition(int x, int y, int height, String character) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.character = character;
		if("E".equals(character))
			shortestPath = List.of(this);
	}
	
	void setNeighboors(MapPosition north, MapPosition south, MapPosition east, MapPosition west) {
		this.north = north;
		this.south = south;
		this.east = east;
		this.west = west;
	}
	
	void checkShortestPath() {
		checkShortestPath(north);
		checkShortestPath(south);
		checkShortestPath(east);
		checkShortestPath(west);
	}
	private void checkShortestPath(MapPosition neighboor) {
		if(neighboor == null)
			return;
		SequencedCollection<MapPosition> shortestPathToNeighboor = neighboor.getShortestPath();
		if(height <= neighboor.getHeight()+1
				&& (shortestPathToNeighboor == null
					|| shortestPath.size()+1 < shortestPathToNeighboor.size())
				) {
			SequencedCollection<MapPosition> newShortestPathToNeighboor = new LinkedList<>();
			newShortestPathToNeighboor.addAll(shortestPath);
			newShortestPathToNeighboor.add(neighboor);
			neighboor.setShortestPath(newShortestPathToNeighboor);
			neighboor.checkShortestPath();
		}
		
	}
	void setShortestPath(SequencedCollection<MapPosition> shortestPath) {
		this.shortestPath = shortestPath;
	}

	int getHeight() {
		return height;
	}

	int getX() {
		return x;
	}

	int getY() {
		return y;
	}

	SequencedCollection<MapPosition> getShortestPath() {
		if(shortestPath == null)
			return null;
		return shortestPath.stream().collect(Collectors.toList());
	}
	
	@Override
	public String toString() {
		return character;
	}
}
