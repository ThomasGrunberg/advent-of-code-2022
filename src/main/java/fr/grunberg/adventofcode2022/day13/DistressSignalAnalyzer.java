package fr.grunberg.adventofcode2022.day13;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SequencedCollection;

public class DistressSignalAnalyzer {
	private static final PacketData firstDivider = parse("[[2]]");
	private static final PacketData secondDivider = parse("[[6]]");
	
	private final List<PacketData> leftPackets;
	private final List<PacketData> rightPackets;

	public DistressSignalAnalyzer(SequencedCollection<String> distressSignal) {
		leftPackets = new ArrayList<>();
		rightPackets = new ArrayList<>();
		Iterator<String> iDistressSignal = distressSignal.iterator();
		while(iDistressSignal.hasNext()) {
			PacketData leftPacket = parse(iDistressSignal.next());
			PacketData rightPacket = parse(iDistressSignal.next());
			leftPackets.add(leftPacket);
			rightPackets.add(rightPacket);
			if(iDistressSignal.hasNext())
				iDistressSignal.next();
		}
		if(leftPackets.size() != rightPackets.size())
			throw new RuntimeException("Non matching signals.");
	}
	
	public int addDividerPacketsSortAndLocate() {
		List<PacketData> allSortedPackets = new LinkedList<>();
		allSortedPackets.addAll(leftPackets);
		allSortedPackets.addAll(rightPackets);
		allSortedPackets.add(firstDivider);
		allSortedPackets.add(secondDivider);
		allSortedPackets = allSortedPackets.stream()
				.sorted((p1, p2) -> {
					Boolean comparison = p1.isRightCorrectOrder(p2);
					if(comparison == null)
						return 0;
					else if(Boolean.TRUE.equals(comparison))
						return -1;
					else
						return 1;
				}).toList();
		int firstDividerIndex = -1;
		int secondDividerIndex = -1;
		for(int i = 1; i <= allSortedPackets.size(); i++) {
			if(firstDivider == allSortedPackets.get(i-1))
				firstDividerIndex = i;
			if(secondDivider == allSortedPackets.get(i-1))
				secondDividerIndex = i;
		}
		return firstDividerIndex * secondDividerIndex;
	}
	
	static PacketData parse(String data) {
		if(!data.startsWith("[") || !data.endsWith("]"))
			throw new RuntimeException("Data unparseable: " + data);
		data = data.replace("10", "A");
		data = data.replace(" ", "");
		data = data.substring(1, data.length()-1);
		PacketDataList root = new PacketDataList(null, new ArrayList<>());
		if(data.isBlank())
			return root;
		String[] splitData = data.split("");
		PacketDataList currentRoot = root;
		for(String token : splitData) {
			switch(token) {
				case "[" :
					PacketDataList newRoot = new PacketDataList(currentRoot, new ArrayList<>());
					currentRoot.values().add(newRoot);
					currentRoot = newRoot;
					break;
				case "]" :
					currentRoot = currentRoot.parent();
					break;
				case "," :
					break;
				case "A" :
					currentRoot.values().add(new PacketDataInteger(currentRoot, 10));
					break;
				default :
					currentRoot.values().add(new PacketDataInteger(currentRoot, Integer.parseInt(token)));
					break;
			}
		}
		return root;
	}

	public int getCorrectIndicesSum() {
		int correctIndices = 0;
		for(int i = 1; i <= leftPackets.size(); i++) {
			if(Boolean.TRUE.equals(leftPackets.get(i-1).isRightCorrectOrder(rightPackets.get(i-1))))
				correctIndices += i;
		}
		return correctIndices;
	}
}
