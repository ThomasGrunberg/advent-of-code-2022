package fr.grunberg.adventofcode2022.day13;

import java.util.List;

interface PacketData {
	List<PacketData> asList();
	PacketDataList parent();
	Boolean isRightCorrectOrder(PacketData rightPacketData);
}
