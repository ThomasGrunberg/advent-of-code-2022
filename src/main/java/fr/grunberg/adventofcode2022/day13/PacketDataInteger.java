package fr.grunberg.adventofcode2022.day13;

import java.util.List;

record PacketDataInteger(PacketDataList parent, int value) implements PacketData {

	@Override
	public List<PacketData> asList() {
		return List.of(this);
	}
	
	@Override
	public String toString() {
		return Integer.valueOf(value).toString();
	}

	@Override
	public Boolean isRightCorrectOrder(PacketData rightPacketData) {
		if(rightPacketData instanceof PacketDataInteger rightPacketDataInteger) {
			if(value() < rightPacketDataInteger.value())
				return Boolean.TRUE;
			if(value() > rightPacketDataInteger.value())
				return Boolean.FALSE;
			return null;
		}
		if(rightPacketData instanceof PacketDataList rightPacketDataList) {
			PacketDataList thisAsAList = new PacketDataList(parent(), List.of(this));
			return thisAsAList.isRightCorrectOrder(rightPacketDataList);
		}
		throw new RuntimeException("What the hell is that type? " + rightPacketData.getClass());
	}
}
