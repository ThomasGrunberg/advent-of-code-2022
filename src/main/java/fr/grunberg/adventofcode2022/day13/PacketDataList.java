package fr.grunberg.adventofcode2022.day13;

import java.util.List;

record PacketDataList(PacketDataList parent, List<PacketData> values) implements PacketData {

	@Override
	public List<PacketData> asList() {
		return values();
	}
	
	@Override
	public String toString() {
		return values.toString();
	}

	@Override
	public Boolean isRightCorrectOrder(PacketData rightPacketData) {
		List<PacketData> rightValues = rightPacketData.asList();
		for(int i = 0; i < Math.max(values.size(), rightValues.size()); i++) {
			if(i >= values.size())
				return Boolean.TRUE;
			if(i >= rightValues.size())
				return Boolean.FALSE;
			Boolean comparison = values.get(i).isRightCorrectOrder(rightValues.get(i));
			if(comparison != null)
				return comparison;
		}
		return null;
	}
}
