package fr.grunberg.adventofcode2022.day2;

import java.util.Arrays;
import java.util.Optional;
import java.util.SequencedCollection;

public class ShiFuMiCalculator {
	private final int bestScore;
	private final int lessSuspiciousScore;
	
	public ShiFuMiCalculator(SequencedCollection<String> strategy) {
		bestScore = strategy.parallelStream()
				.mapToInt(l -> getBestScore(l))
				.sum();
		lessSuspiciousScore = strategy.parallelStream()
				.mapToInt(l -> getLessSuspiciousScore(l))
				.sum();
	}
	
	private int getBestScore(String strategyLine) {
		if(strategyLine == null || strategyLine.length() != 3)
			return 0;
		String[] characters = strategyLine.split("");
		Optional<ShiFuMiShape> opponentShape = Arrays.asList(ShiFuMiShape.values()).stream()
				.filter(s -> s.getOpponentCode().equals(characters[0]))
				.findAny();
		Optional<ShiFuMiShape> playerShape = Arrays.asList(ShiFuMiShape.values()).stream()
				.filter(s -> s.getPlayerCode().equals(characters[2]))
				.findAny();
		if(opponentShape.isEmpty() || playerShape.isEmpty()) {
			throw new IllegalArgumentException("What is " + strategyLine);
		}
		ShiFuMiResult result = playerShape.get().getResult(opponentShape.get());
		return playerShape.get().getScore()
				+ result.getScore();
	}
	
	private int getLessSuspiciousScore(String strategyLine) {
		if(strategyLine == null || strategyLine.length() != 3)
			return 0;
		String[] characters = strategyLine.split("");
		Optional<ShiFuMiShape> opponentShape = Arrays.asList(ShiFuMiShape.values()).stream()
				.filter(s -> s.getOpponentCode().equals(characters[0]))
				.findAny();
		Optional<ShiFuMiResult> result = Arrays.asList(ShiFuMiResult.values()).stream()
				.filter(r -> r.getCode().equals(characters[2]))
				.findAny();
		ShiFuMiShape playerShape = opponentShape.get().getResult(result.get());
		if(opponentShape.isEmpty() || result.isEmpty()) {
			throw new IllegalArgumentException("What is " + strategyLine);
		}
		return playerShape.getScore()
				+ result.get().getScore();
	}
	
	public int getBestScore() {
		return bestScore;
	}
	public int getLessSuspiciousScore() {
		return lessSuspiciousScore;
	}
}
