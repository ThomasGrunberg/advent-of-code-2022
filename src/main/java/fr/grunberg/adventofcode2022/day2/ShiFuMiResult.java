package fr.grunberg.adventofcode2022.day2;

public enum ShiFuMiResult {
	VICTORY("Z", 6),
	DRAW("Y", 3),
	DEFEAT("X", 0);

	private final String code;
	private final int score;

	ShiFuMiResult(String code, int score) {
		this.code = code;
		this.score = score;
	}

	public int getScore() {
		return score;
	}

	public String getCode() {
		return code;
	}
}
