package fr.grunberg.adventofcode2022.day2;

public enum ShiFuMiShape {
	ROCK("A", "X", 1),
	PAPER("B", "Y", 2),
	SCISSOR("C", "Z", 3);
	
	private final String opponentCode;
	private final String playerCode;
	private final int score;
	
	ShiFuMiShape(String opponentCode, String playerCode, int score) {
		this.opponentCode = opponentCode;
		this.playerCode = playerCode;
		this.score = score;
	}

	public String getPlayerCode() {
		return playerCode;
	}
	public String getOpponentCode() {
		return opponentCode;
	}
	public int getScore() {
		return score;
	}
	public ShiFuMiResult getResult(ShiFuMiShape opponentShape) {
		if(opponentShape == this)
			return ShiFuMiResult.DRAW;
		switch(this) {
			case ROCK : 	return (opponentShape == SCISSOR ? ShiFuMiResult.VICTORY : ShiFuMiResult.DEFEAT);
			case PAPER : 	return (opponentShape == ROCK ? ShiFuMiResult.VICTORY : ShiFuMiResult.DEFEAT);
			case SCISSOR : 	return (opponentShape == PAPER ? ShiFuMiResult.VICTORY : ShiFuMiResult.DEFEAT);
			default : throw new IllegalArgumentException("What the hell is " + this);
		}
	}

	ShiFuMiShape getResult(ShiFuMiResult result) {
		if(result == ShiFuMiResult.DRAW)
			return this;
		switch(this) {
			case ROCK : 	return (result == ShiFuMiResult.VICTORY ? PAPER : SCISSOR);
			case PAPER : 	return (result == ShiFuMiResult.VICTORY ? SCISSOR : ROCK);
			case SCISSOR : 	return (result == ShiFuMiResult.VICTORY ? ROCK : PAPER);
			default : throw new IllegalArgumentException("What the hell is " + this);
	}
	}
}
