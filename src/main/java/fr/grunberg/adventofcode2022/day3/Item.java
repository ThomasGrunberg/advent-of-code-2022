package fr.grunberg.adventofcode2022.day3;

public record Item(String letter) {
	public int getPriority() {
		int asciiCode = letter.charAt(0);
		if(asciiCode > 96)
			return asciiCode - 96;
		return asciiCode - 64 + 26;
	}
}
