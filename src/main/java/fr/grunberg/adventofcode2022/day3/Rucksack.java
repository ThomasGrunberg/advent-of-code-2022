package fr.grunberg.adventofcode2022.day3;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Rucksack {
	private final String content;
	private final Collection<Item> firstCompartment;
	private final Collection<Item> secondCompartment;
	
	public Rucksack(String content) {
		this.content = content;
		if(content == null || content.length() == 0  || content.length()%2 != 0)
			throw new IllegalArgumentException("That's not valid : " + content);
		firstCompartment = 
			Arrays.asList(content.substring(0, content.length()/2).split(""))
				.stream()
				.map(l -> new Item(l))
				.toList();
		secondCompartment = 
				Arrays.asList(content.substring(content.length()/2).split(""))
					.stream()
					.map(l -> new Item(l))
					.toList();
	}
	
	public Item getWronglySortedItem() {
		Optional<Item> duplicateItem = firstCompartment.stream()
			.filter(i -> secondCompartment.contains(i))
			.findAny();
		if(duplicateItem.isEmpty())
			throw new IllegalArgumentException("That's not valid : " + content);
		return duplicateItem.get();
	}
	

	private Collection<Item> getItems() {
		return Stream.of(firstCompartment, secondCompartment)
				.flatMap(s -> s.stream())
				.collect(Collectors.toSet());
	}

	public static Item getBadge(Collection<Rucksack> rucksacks) {
		Iterator<Rucksack> iRucksacks = rucksacks.iterator();
		Collection<Item> itemsInFirstRucksack = iRucksacks.next().getItems();
		Collection<Item> itemsInSecondRucksack = iRucksacks.next().getItems();
		Collection<Item> itemsInThirdRucksack = iRucksacks.next().getItems();
		Optional<Item> badge = itemsInFirstRucksack.stream()
				.filter(i -> itemsInSecondRucksack.contains(i))
				.filter(i -> itemsInThirdRucksack.contains(i))
				.findFirst();
		if(badge.isEmpty()) {
			throw new IllegalArgumentException("Invalid group : " + rucksacks);
		}
		return badge.get();
	}
}
