package fr.grunberg.adventofcode2022.day4;

public class Task {
	private final int firstSection;
	private final int lastSection;
	
	public Task(String schedule) {
		String[] sections = schedule.split("-");
		firstSection = Integer.parseInt(sections[0]);
		lastSection = Integer.parseInt(sections[1]);
	}

	public int getFirstSection() {
		return firstSection;
	}

	public int getLastSection() {
		return lastSection;
	}
	
	public boolean contains(Task otherTask) {
		if(this.firstSection <= otherTask.getFirstSection()
				&& this.lastSection >= otherTask.getLastSection()
				)
			return true;
		return false;
	}
	
	public boolean overlaps(Task otherTask) {
		if(this.firstSection <= otherTask.getFirstSection()
				&& this.lastSection >= otherTask.getFirstSection()
				)
			return true;
		if(this.firstSection <= otherTask.getLastSection()
				&& this.lastSection >= otherTask.getLastSection()
				)
			return true;
		return false;
	}
}
