package fr.grunberg.adventofcode2022.day4;

public class TaskPair {
	private final Task firstTask;
	private final Task secondTask;
	
	public TaskPair(String tasks) {
		if(tasks == null || !tasks.contains(",")) {
			throw new IllegalArgumentException("Invalid schedule " + tasks);
		}
		String[] tasksAsString = tasks.split(",");
		firstTask = new Task(tasksAsString[0]);
		secondTask = new Task(tasksAsString[1]);
	}
	
	public int getTaskCompleteOverlap() {
		if(firstTask.contains(secondTask) || secondTask.contains(firstTask))
			return 1;
		return 0;
	}
	
	public int getTaskPartialOverlap() {
		if(firstTask.overlaps(secondTask) || secondTask.overlaps(firstTask))
			return 1;
		return 0;
	}
}
