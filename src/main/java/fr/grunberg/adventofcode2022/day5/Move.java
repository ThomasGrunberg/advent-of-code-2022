package fr.grunberg.adventofcode2022.day5;

import java.util.LinkedList;
import java.util.List;
import java.util.SequencedCollection;

public class Move {
	final int numbersOfCratesToMove;
	final int pileNumberFrom;
	final int pileNumberTo;
	final boolean activeCrateMover9001MultiMove;

	public Move(String instruction, boolean activeCrateMover9001MultiMove) {
		this.activeCrateMover9001MultiMove = activeCrateMover9001MultiMove;
		String[] instructionSteps = instruction.split(" ");
		if(instructionSteps.length != 6
				|| !instructionSteps[0].equals("move")
				|| !instructionSteps[2].equals("from")
				|| !instructionSteps[4].equals("to")
				)
			throw new IllegalArgumentException("Invalid move : " + instruction);
		try {
			numbersOfCratesToMove = Integer.parseInt(instructionSteps[1]);
			pileNumberFrom = Integer.parseInt(instructionSteps[3]);
			pileNumberTo = Integer.parseInt(instructionSteps[5]);
		}
		catch(NumberFormatException e) {
			throw new IllegalArgumentException("Invalid move : " + instruction, e);
		}
	}
	
	public List<SequencedCollection<String>> apply(List<SequencedCollection<String>> pilesOfCrates) {
		SequencedCollection<String> pileFrom = pilesOfCrates.get(pileNumberFrom-1);
		SequencedCollection<String> pileTo = pilesOfCrates.get(pileNumberTo-1);
		if(pileFrom.size() < numbersOfCratesToMove)
			throw new IllegalArgumentException("Invalid move" + this);
		if(activeCrateMover9001MultiMove) {
			SequencedCollection<String> cratesMoved = new LinkedList<>();
			for(int i = 0; i < numbersOfCratesToMove; i++) {
				cratesMoved.add(pileFrom.removeLast());
			}
			while(!cratesMoved.isEmpty())
				pileTo.add(cratesMoved.removeLast());
		}
		else {
			for(int i = 0; i < numbersOfCratesToMove; i++) {
				pileTo.add(pileFrom.removeLast());
			}
		}
		return pilesOfCrates;
	}
}
