package fr.grunberg.adventofcode2022.day5;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SequencedCollection;
import java.util.stream.Collectors;

public class Procedure {
	private final List<SequencedCollection<String>> pilesOfCrates;
	
	public Procedure(SequencedCollection<String> procedure, boolean activeCrateMover9001MultiMove) {
		pilesOfCrates = decodePilesOfCrates(procedure);
		for(String instruction : procedure) {
			if(instruction.startsWith("move")) {
				Move move = new Move(instruction, activeCrateMover9001MultiMove);
				move.apply(pilesOfCrates);
			}
		}
	}

	private List<SequencedCollection<String>> decodePilesOfCrates(SequencedCollection<String> procedure) {
		SequencedCollection<String> undecodedPilesOfCrates = decodePilesOfCratesAsString(procedure).reversed();
		Iterator<String> iUndecodedPilesOfCrates = undecodedPilesOfCrates.iterator();
		List<SequencedCollection<String>> initialPilesOfCrates = new ArrayList<>();
		iUndecodedPilesOfCrates.next();
		while(iUndecodedPilesOfCrates.hasNext()) {
			List<String> cratesInLine = decodeLineOfCrates(iUndecodedPilesOfCrates.next());
			for(int p = 0; p < cratesInLine.size(); p++) {
				if(initialPilesOfCrates.size() <= p)
					initialPilesOfCrates.add(new LinkedList<>());
				if(cratesInLine.get(p) != null && !cratesInLine.get(p).isBlank())
					initialPilesOfCrates.get(p).add(cratesInLine.get(p));
			}
		}
		return initialPilesOfCrates;
	}

	private List<String> decodeLineOfCrates(String undecodedPilesOfCrate) {
		while(undecodedPilesOfCrate.length()%4 != 0)
			undecodedPilesOfCrate = undecodedPilesOfCrate + " ";
		int lastIndex = 4;
		List<String> crates = new ArrayList<>(undecodedPilesOfCrate.length()/4);
		while(undecodedPilesOfCrate.length() >= lastIndex) {
			crates.add(
					undecodedPilesOfCrate
					.substring(lastIndex-4, lastIndex)
					.trim()
					.replace("[", "")
					.replace("]", "")
				);
			lastIndex += 4;
		}
		return crates;
	}

	private SequencedCollection<String> decodePilesOfCratesAsString(SequencedCollection<String> procedure) {
		SequencedCollection<String> undecodedPilesOfCrates = new ArrayList<>();
		for(String line : procedure) {
			if(line.isBlank())
				break;
			undecodedPilesOfCrates.add(line);
		}
		return undecodedPilesOfCrates;
	}

	public String getAvailableCrates() {
		return pilesOfCrates.stream()
				.map(p -> p.getLast())
				.collect(Collectors.joining());
	}
}
