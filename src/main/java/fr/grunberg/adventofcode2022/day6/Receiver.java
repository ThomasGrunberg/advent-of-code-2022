package fr.grunberg.adventofcode2022.day6;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.SequencedCollection;

public class Receiver {
	private static final int StartOfPacketSize = 4;
	private static final int StartOfMessageSize = 14;
	
	private int findMarker(String message, int distinctNumber) {
		List<String> characters = Arrays.asList(message.split(""));
		SequencedCollection<String> lastFourCharacters = new LinkedList<>();
		for(int i = 0; i < characters.size(); i++) {
			lastFourCharacters.add(characters.get(i));
			if(lastFourCharacters.size() > distinctNumber)
				lastFourCharacters.removeFirst();
			if(distinctNumber == lastFourCharacters.stream().distinct().count())
				return i+1;
		}
		return -1;
	}
	
	public int findStartOfPacketMarker(String message) {
		return findMarker(message, StartOfPacketSize);
	}
	
	public int findStartOfMessageMarker(String message) {
		return findMarker(message, StartOfMessageSize);
	}
}
