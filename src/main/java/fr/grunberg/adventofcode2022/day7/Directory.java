package fr.grunberg.adventofcode2022.day7;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public class Directory implements Inode {
	private final String name;
	private final Directory parent;
	private final Collection<Inode> content = new ArrayList<>();
	
	public Directory(String name, Directory parent) {
		this.name = name;
		this.parent = parent;
	}
	
	public void addInode(Inode newElement) {
		content.add(newElement);
	}

	@Override
	public int getSize() {
		return content.stream().mapToInt(Inode::getSize).sum();
	}
	@Override
	public Directory getParent() {
		return parent;
	}
	@Override
	public String getName() {
		return name;
	}
	@Override
	public Collection<Directory> getSmallDirectories(int sizeThreshold) {
		boolean thisDirectoryBelowThreshold = (getSize() <= sizeThreshold);
		Collection<Directory> smallDirectories = content.stream()
			.filter(c -> c instanceof Directory)
			.map(c -> ((Directory)c).getSmallDirectories(sizeThreshold))
			.flatMap(c -> c.stream())
			.collect(Collectors.toList());
		if(thisDirectoryBelowThreshold)
			smallDirectories.add(this);
		return smallDirectories;
	}

	public Directory findSubDirectory(String targetDirectory) {
		Optional<Directory> subDirectory = content.stream()
				.filter(c -> c.getName().equals(targetDirectory))
				.filter(c -> c instanceof Directory)
				.map(c -> (Directory) c)
				.findAny();
		if(subDirectory.isEmpty())
			throw new IllegalArgumentException("Cannot find " + targetDirectory + " in " + this.getName());
		return subDirectory.get();
	}

	public Collection<Directory> getSubdirectories() {
		Collection<Directory> directories = 
			content.stream()
				.filter(c -> c instanceof Directory)
				.map(c -> (Directory) c)
				.map(c -> c.getSubdirectories())
				.flatMap(s -> s.stream())
				.collect(Collectors.toList());
		directories.add(this);
		return directories;
	}
	
	@Override
	public String toString() {
		return "> " + getName() + " (" + getSize() + ")"; 
	}
}
