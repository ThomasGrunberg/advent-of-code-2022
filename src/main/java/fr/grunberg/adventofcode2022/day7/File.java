package fr.grunberg.adventofcode2022.day7;

import java.util.Collection;
import java.util.Collections;

public class File implements Inode {
	private final String name;
	private final Directory parent;
	private final int size;
	
	public File(String name, Directory parent, int size) {
		this.name = name;
		this.parent = parent;
		this.size = size;
	}

	@Override
	public int getSize() {
		return size;
	}
	@Override
	public Directory getParent() {
		return parent;
	}
	@Override
	public String getName() {
		return name;
	}
	@Override
	public Collection<Directory> getSmallDirectories(int sizeThreshold) {
		return Collections.emptyList();
	}
	
	@Override
	public String toString() {
		return "| " + getName() + " (" + getSize() + ")"; 
	}
}
