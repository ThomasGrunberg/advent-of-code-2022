package fr.grunberg.adventofcode2022.day7;

import java.util.SequencedCollection;

public class FileSystem {
	private final Directory root;
	private final int totalSize;
	
	public FileSystem(SequencedCollection<String> fsExploration, int totalSize) {
		this.totalSize = totalSize;
		root = new Directory("/", null);
		Directory currentDirectory = root;
		for(String cliLine : fsExploration) {
			if(cliLine.equals("$ cd /"))
				currentDirectory = root;
			else if(cliLine.equals("$ cd .."))
				currentDirectory = currentDirectory.getParent();
			else if(cliLine.startsWith("$ cd ")) {
				String targetDirectory = cliLine.replace("$ cd ", "");
				currentDirectory = currentDirectory.findSubDirectory(targetDirectory);
			}
			else if(cliLine.equals("$ ls")) {
				// nothing to do
			}
			else {
				String[] inodeDescription = cliLine.split(" ");
				if(inodeDescription[0].equals("dir")) {
					currentDirectory.addInode(new Directory(inodeDescription[1], currentDirectory));
				}
				else {
					int inodeSize = Integer.parseInt(inodeDescription[0]);
					currentDirectory.addInode(new File(inodeDescription[1], currentDirectory, inodeSize));
				}
			}
		}
	}

	public int getSmallDirectoriesSize() {
		return root.getSmallDirectories(100_000)
			.stream()
			.mapToInt(d -> d.getSize())
			.sum();
	}
	
	public Directory getDirectoryToRemove(int freeSizeToGet) {
		return getSmallestDirectoryAboveThreshold(freeSizeToGet - getFreeSpace());
	}
	private Directory getSmallestDirectoryAboveThreshold(int threshold) {
		return root.getSubdirectories()
				.stream()
				.filter(d -> d.getSize() > threshold)
				.sorted(
						(dir1, dir2) 
	                     -> dir1.getSize() - dir2.getSize()
						)
				.findFirst()
				.get();
	}
	private int getFreeSpace() {
		return totalSize - root.getSize();
	}
}
