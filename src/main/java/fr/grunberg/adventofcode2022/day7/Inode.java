package fr.grunberg.adventofcode2022.day7;

import java.util.Collection;

public interface Inode {
	public String getName();
	public Directory getParent();
	public int getSize();
	public Collection<Directory> getSmallDirectories(int sizeThreshold);
}
