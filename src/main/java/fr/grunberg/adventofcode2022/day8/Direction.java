package fr.grunberg.adventofcode2022.day8;

public enum Direction {
	NORTH,
	SOUTH,
	EAST,
	WEST
}
