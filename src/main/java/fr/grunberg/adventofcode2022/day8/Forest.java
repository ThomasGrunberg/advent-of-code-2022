package fr.grunberg.adventofcode2022.day8;

import java.util.ArrayList;
import java.util.Collection;
import java.util.SequencedCollection;

public class Forest {
	private final Collection<Tree> unsortedTrees;
	private final Tree[][] arrangedTrees;

	public Forest(SequencedCollection<String> simpleForest) {
		int sizeX = simpleForest.getFirst().length();
		int sizeY = simpleForest.size();
		unsortedTrees = new ArrayList<>(sizeX * sizeY);
		arrangedTrees = new Tree[sizeX][sizeY];
		int x = 0;
		for(String row : simpleForest) {
			int y = 0;
			for(String singleTreeSizeAsString : row.split("")) {
				Tree tree = new Tree(Byte.parseByte(singleTreeSizeAsString));
				unsortedTrees.add(tree);
				arrangedTrees[x][y] = tree;
				y++;
			}
			x++;
		}
		for(x = 0; x < sizeX; x++) {
			for(int y = 0; y < sizeY; y++) {
				if(x > 0)
					arrangedTrees[x][y].setNorthTree(arrangedTrees[x-1][y]);
				if(x < sizeX-1)
					arrangedTrees[x][y].setSouthTree(arrangedTrees[x+1][y]);
				if(y > 0)
					arrangedTrees[x][y].setWestTree(arrangedTrees[x][y-1]);
				if(y < sizeY-1)
					arrangedTrees[x][y].setEastTree(arrangedTrees[x][y+1]);
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuilder printedForest = new StringBuilder();

		for(int x = 0; x < arrangedTrees.length; x++) {
			for(int y = 0; y < arrangedTrees[x].length; y++) {
				printedForest.append(arrangedTrees[x][y].getSize());
			}
			printedForest.append("\n");
		}
		printedForest.append("\n");

		for(int x = 0; x < arrangedTrees.length; x++) {
			for(int y = 0; y < arrangedTrees[x].length; y++) {
				printedForest.append(arrangedTrees[x][y].calculateScenicScore());
			}
			printedForest.append("\n");
		}

		return printedForest.toString();
	}

	public int countVisibleTrees() {
		return (int) unsortedTrees.stream()
			.filter(t -> t.isVisible())
			.count();
	}
	
	public int getHighestScenicScore() {
		return (int) unsortedTrees.stream()
				.mapToInt(t -> t.calculateScenicScore())
				.max()
				.getAsInt();
	}
}
