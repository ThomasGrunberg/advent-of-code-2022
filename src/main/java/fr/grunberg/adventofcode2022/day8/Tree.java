package fr.grunberg.adventofcode2022.day8;

public class Tree {
	private final byte size;
	private Tree northTree;
	private Tree southTree;
	private Tree eastTree;
	private Tree westTree;
	
	public Tree(byte size) {
		this.size = size;
	}

	public Tree getNorthTree() {
		return northTree;
	}

	void setNorthTree(Tree northTree) {
		this.northTree = northTree;
	}

	public Tree getSouthTree() {
		return southTree;
	}

	void setSouthTree(Tree southTree) {
		this.southTree = southTree;
	}

	public Tree getEastTree() {
		return eastTree;
	}

	final void setEastTree(Tree eastTree) {
		this.eastTree = eastTree;
	}

	public Tree getWestTree() {
		return westTree;
	}

	void setWestTree(Tree westTree) {
		this.westTree = westTree;
	}

	public byte getSize() {
		return size;
	}

	public boolean isVisible() {
		if(northTree == null
			|| southTree == null
			||  eastTree == null
			||  westTree == null
				)
			return true;
		return
			   northTree.isSmaller(this.size, Direction.NORTH)
			|| southTree.isSmaller(this.size, Direction.SOUTH)
			||  eastTree.isSmaller(this.size, Direction.EAST)
			||  westTree.isSmaller(this.size, Direction.WEST);
	}

	private Tree getTree(Direction direction) {
		switch(direction) {
			case NORTH : return northTree;
			case SOUTH : return southTree;
			case WEST  : return  westTree;
			case EAST  : return  eastTree;
		}
		throw new IllegalArgumentException("This shouldn't happen");
	}
	
	private boolean isSmaller(byte sizeToSee, Direction direction) {
		if(this.size >= sizeToSee)
			return false;
		if(getTree(direction) == null)
			return true;
		return getTree(direction).isSmaller(sizeToSee, direction);
	}
	
	public int calculateScenicScore() {
		if(northTree == null
				|| southTree == null
				||  eastTree == null
				||  westTree == null
					)
			return 0;
		return this.getVisibleTrees(size, Direction.NORTH)
			* this.getVisibleTrees(size, Direction.SOUTH)
			* this.getVisibleTrees(size, Direction.WEST)
			* this.getVisibleTrees(size, Direction.EAST);
	}

	int getVisibleTrees(byte sizeToSee, Direction direction) {
		Tree directionTree = getTree(direction);
		if(directionTree == null)
			return 0;
		if(directionTree.getSize() >= sizeToSee)
			return 1;
		return 1 + directionTree.getVisibleTrees(sizeToSee, direction);
	}
	
	@Override
	public String toString() {
		return String.valueOf(size);
	}
}
