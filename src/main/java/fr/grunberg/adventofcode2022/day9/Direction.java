package fr.grunberg.adventofcode2022.day9;

public enum Direction {
	UP("U"),
	DOWN("D"),
	LEFT("L"),
	RIGHT("R");
	
	private final String code;
	
	Direction(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
