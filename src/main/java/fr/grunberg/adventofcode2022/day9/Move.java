package fr.grunberg.adventofcode2022.day9;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

public class Move {
	private final Direction direction;
	private final int distance;
	
	public Move(Direction direction, int distance) {
		this.direction = direction;
		this.distance = distance;
	}
	
	public Move(String moveDescription) {
		String[] moveDescriptionSplit = moveDescription.split(" ");
		direction = Arrays.asList(Direction.values())
			.stream()
			.filter(d -> d.getCode().equals(moveDescriptionSplit[0]))
			.findAny()
			.get();
		distance = Integer.parseInt(moveDescriptionSplit[1]);
	}
	
	public Collection<Move> toAtomicMoves() {
		Collection<Move> atomicMoves = new LinkedList<>();
		Move singleMove = new Move(direction, 1);
		for(int i = 0; i < distance; i++)
			atomicMoves.add(singleMove);
		return atomicMoves;
	}

	public Position apply(Position startPosition) {
		switch(direction) {
		case DOWN :		return new Position(startPosition.x(), startPosition.y()-1);
		case UP :		return new Position(startPosition.x(), startPosition.y()+1);
		case LEFT :		return new Position(startPosition.x()-1, startPosition.y());
		case RIGHT : 	return new Position(startPosition.x()+1, startPosition.y());
		default :		return startPosition;
		}
	}
	
	@Override
	public String toString() {
		return distance + " " + direction.name();
	}
}
