package fr.grunberg.adventofcode2022.day9;

public record Position(int x, int y) {
}
