package fr.grunberg.adventofcode2022.day9;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.SequencedCollection;

public class RopeBridge {
	private final Collection<Position> allHeadPositions;
	private final Collection<Position> allTailPositions;
	private final List<Position> ropePosition;

	public RopeBridge(SequencedCollection<String> ropeMovements, int ropeSize) {
		SequencedCollection<Move> moves = ropeMovements.stream()
				.map(rm -> new Move(rm))
				.map(m -> m.toAtomicMoves())
				.flatMap(s -> s.stream())
				.toList();
		ropePosition = new ArrayList<>(ropeSize);
		for(int i = 0; i < ropeSize; i++)
			ropePosition.add(new Position(0, 0));
		allHeadPositions = new HashSet<>();
		allTailPositions = new HashSet<>();
		allHeadPositions.add(ropePosition.getFirst());
		allTailPositions.add(ropePosition.getLast());
		for(Move move : moves) {
			ropePosition.set(0, move.apply(ropePosition.getFirst()));
			Position previousRopePiece = ropePosition.getFirst();
			for(int i = 1; i < ropeSize; i++) {
				Position nextRopePiece = ropePosition.get(i);
				if(Math.abs(previousRopePiece.x() - nextRopePiece.x()) > 1
						|| Math.abs(previousRopePiece.y() - nextRopePiece.y()) > 1
						) {
					if(previousRopePiece.x() > nextRopePiece.x())
						nextRopePiece = new Position(nextRopePiece.x()+1, nextRopePiece.y());
					else if(previousRopePiece.x() < nextRopePiece.x())
						nextRopePiece = new Position(nextRopePiece.x()-1, nextRopePiece.y());
					if(previousRopePiece.y() > nextRopePiece.y())
						nextRopePiece = new Position(nextRopePiece.x(), nextRopePiece.y()+1);
					else if(previousRopePiece.y() < nextRopePiece.y())
						nextRopePiece = new Position(nextRopePiece.x(), nextRopePiece.y()-1);
					ropePosition.set(i, nextRopePiece);
					}
				previousRopePiece = nextRopePiece;
				}
			allHeadPositions.add(ropePosition.getFirst());
			allTailPositions.add(ropePosition.getLast());
		}
	}

	public int countAllPositions() {
		return allTailPositions.size();
	}
}
