package fr.grunberg.adventofcode2022;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.SequencedCollection;

public class InputReader {
	private static final String resourcePath = "./target/test-classes/inputs/";
	
	public static SequencedCollection<String> readFile(String fileName) throws IOException {
		Path file = FileSystems.getDefault().getPath(resourcePath + fileName);
		return Files.readAllLines(file, StandardCharsets.UTF_8);
	}
}
