package fr.grunberg.adventofcode2022.day1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;

import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class InventoryTest {
	private final SequencedCollection<String> simpleRegister = List.of("1000",
			"2000",
			"3000",
			"",
			"4000",
			"",
			"5000",
			"6000",
			"",
			"7000",
			"8000",
			"9000",
			"",
			"10000"
			);
	
	@Test
	public void getMaxCalories_SimpleTest() {
		Inventory inventory = new Inventory(simpleRegister);
		assertEquals(24000, inventory.getMaxCalories());
	}
	
	@Test
	public void getMaxCalories_InputTest() throws IOException {
		SequencedCollection<String> register = InputReader.readFile("day1.txt");
		Inventory inventory = new Inventory(register);
		assertEquals(72718, inventory.getMaxCalories());
	}

	@Test
	public void getThreeMaxCalories_SimpleTest() {
		Inventory inventory = new Inventory(simpleRegister);
		int threeMaxCalories = inventory.getThreeMaxCalories();
		assertEquals(45000, threeMaxCalories);
	}

	@Test
	public void getThreeMaxCalories_InputTest() throws IOException {
		SequencedCollection<String> register = InputReader.readFile("day1.txt");
		Inventory inventory = new Inventory(register);
		int threeMaxCalories = inventory.getThreeMaxCalories();
		assertEquals(213089, threeMaxCalories);
	}
}
