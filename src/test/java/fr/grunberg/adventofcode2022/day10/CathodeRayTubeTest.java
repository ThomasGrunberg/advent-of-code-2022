package fr.grunberg.adventofcode2022.day10;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class CathodeRayTubeTest {
	private final SequencedCollection<String> simpleInstructions = List.of(
			"addx 15",
			"addx -11",
			"addx 6",
			"addx -3",
			"addx 5",
			"addx -1",
			"addx -8",
			"addx 13",
			"addx 4",
			"noop",
			"addx -1",
			"addx 5",
			"addx -1",
			"addx 5",
			"addx -1",
			"addx 5",
			"addx -1",
			"addx 5",
			"addx -1",
			"addx -35",
			"addx 1",
			"addx 24",
			"addx -19",
			"addx 1",
			"addx 16",
			"addx -11",
			"noop",
			"noop",
			"addx 21",
			"addx -15",
			"noop",
			"noop",
			"addx -3",
			"addx 9",
			"addx 1",
			"addx -3",
			"addx 8",
			"addx 1",
			"addx 5",
			"noop",
			"noop",
			"noop",
			"noop",
			"noop",
			"addx -36",
			"noop",
			"addx 1",
			"addx 7",
			"noop",
			"noop",
			"noop",
			"addx 2",
			"addx 6",
			"noop",
			"noop",
			"noop",
			"noop",
			"noop",
			"addx 1",
			"noop",
			"noop",
			"addx 7",
			"addx 1",
			"noop",
			"addx -13",
			"addx 13",
			"addx 7",
			"noop",
			"addx 1",
			"addx -33",
			"noop",
			"noop",
			"noop",
			"addx 2",
			"noop",
			"noop",
			"noop",
			"addx 8",
			"noop",
			"addx -1",
			"addx 2",
			"addx 1",
			"noop",
			"addx 17",
			"addx -9",
			"addx 1",
			"addx 1",
			"addx -3",
			"addx 11",
			"noop",
			"noop",
			"addx 1",
			"noop",
			"addx 1",
			"noop",
			"noop",
			"addx -13",
			"addx -19",
			"addx 1",
			"addx 3",
			"addx 26",
			"addx -30",
			"addx 12",
			"addx -1",
			"addx 3",
			"addx 1",
			"noop",
			"noop",
			"noop",
			"addx -9",
			"addx 18",
			"addx 1",
			"addx 2",
			"noop",
			"noop",
			"addx 9",
			"noop",
			"noop",
			"noop",
			"addx -1",
			"addx 2",
			"addx -37",
			"addx 1",
			"addx 3",
			"noop",
			"addx 15",
			"addx -21",
			"addx 22",
			"addx -6",
			"addx 1",
			"noop",
			"addx 2",
			"addx 1",
			"noop",
			"addx -10",
			"noop",
			"noop",
			"addx 20",
			"addx 1",
			"addx 2",
			"addx 2",
			"addx -6",
			"addx -11",
			"noop",
			"noop",
			"noop"
			);

	@Test
	public void calculateSignalStrength_simpleInstructions() {
		CPU cpu = new CPU(simpleInstructions);
		assertEquals(13140, cpu.calculateSignalStrength());
	}
	
	@Test
	public void calculateSignalStrength_inputInstructions() throws IOException {
		SequencedCollection<String> inputInstructions = InputReader.readFile("day10.txt");
		CPU cpu = new CPU(inputInstructions);
		assertEquals(15260, cpu.calculateSignalStrength());
	}

	@Test
	public void renderScreen_simpleInstructions() {
		SequencedCollection<String> screen = List.of(
				"##..##..##..##..##..##..##..##..##..##..",
				"###...###...###...###...###...###...###.",
				"####....####....####....####....####....",
				"#####.....#####.....#####.....#####.....",
				"######......######......######......####",
				"#######.......#######.......#######....."
				);
		CPU cpu = new CPU(simpleInstructions);
		assertEquals(screen.stream().collect(Collectors.joining()), cpu.renderScreen().stream().collect(Collectors.joining()));
	}
	
	@Test
	public void render_inputInstructions() throws IOException {
		SequencedCollection<String> screen = List.of(
				"###...##..#..#.####..##..#....#..#..##..",
				"#..#.#..#.#..#.#....#..#.#....#..#.#..#.",
				"#..#.#....####.###..#....#....#..#.#....",
				"###..#.##.#..#.#....#.##.#....#..#.#.##.",
				"#....#..#.#..#.#....#..#.#....#..#.#..#.",
				"#.....###.#..#.#.....###.####..##...###."
				);
		SequencedCollection<String> inputInstructions = InputReader.readFile("day10.txt");
		CPU cpu = new CPU(inputInstructions);
		/*for(String line : cpu.renderScreen())
			System.out.println(line);*/
		assertEquals(screen.stream().collect(Collectors.joining()), cpu.renderScreen().stream().collect(Collectors.joining()));
	}
}
