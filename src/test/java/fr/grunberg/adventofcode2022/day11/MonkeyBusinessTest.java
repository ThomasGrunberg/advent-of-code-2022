package fr.grunberg.adventofcode2022.day11;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;
import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class MonkeyBusinessTest {
	private final SequencedCollection<String> simpleMonkeys = List.of("""
			Monkey 0:
				  Starting items: 79, 98
				  Operation: new = old * 19
				  Test: divisible by 23
				    If true: throw to monkey 2
				    If false: throw to monkey 3

			Monkey 1:
			  Starting items: 54, 65, 75, 74
			  Operation: new = old + 6
			  Test: divisible by 19
			    If true: throw to monkey 2
			    If false: throw to monkey 0

			Monkey 2:
			  Starting items: 79, 60, 97
			  Operation: new = old * old
			  Test: divisible by 13
			    If true: throw to monkey 1
			    If false: throw to monkey 3

			Monkey 3:
			  Starting items: 74
			  Operation: new = old + 3
			  Test: divisible by 17
			    If true: throw to monkey 0
			    If false: throw to monkey 1"""
			.split("\n")
			);

	@Test
	public void calculateMonkeyBusiness_simpleInstructions() {
		MonkeyHerd monkeys = new MonkeyHerd(simpleMonkeys, true, 20);
		assertEquals(10605, monkeys.getMonkeyBusiness());
	}
	
	@Test
	public void calculateSignalStrength_inputInstructions() throws IOException {
		SequencedCollection<String> inputMonkeys = InputReader.readFile("day11.txt");
		MonkeyHerd monkeys = new MonkeyHerd(inputMonkeys, true, 20);
		assertEquals(151312, monkeys.getMonkeyBusiness());
	}

	@Test
	public void calculateMonkeyBusinessVeryLong_simpleInstructions() {
		MonkeyHerd monkeys = new MonkeyHerd(simpleMonkeys, false, 10_000);
		assertEquals(2713310158L, monkeys.getMonkeyBusiness());
	}

	@Test
	public void calculateMonkeyBusinessVeryLong_inputInstructions() throws IOException {
		SequencedCollection<String> inputMonkeys = InputReader.readFile("day11.txt");
		MonkeyHerd monkeys = new MonkeyHerd(inputMonkeys, false, 10_000);
		assertEquals(51382025916L, monkeys.getMonkeyBusiness());
	}
}
