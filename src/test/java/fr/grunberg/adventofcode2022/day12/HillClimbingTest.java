package fr.grunberg.adventofcode2022.day12;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;
import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class HillClimbingTest {
	private final SequencedCollection<String> simpleHill = List.of("""
			Sabqponm
			abcryxxl
			accszExk
			acctuvwj
			abdefghi"""
			.split("\n")
			);

	@Test
	public void findShortestHillPath_simpleInstructions() {
		HillClimbingAlgorithm hillClimbing = new HillClimbingAlgorithm(simpleHill);
		assertEquals(31, hillClimbing.getShortestPath().size()-1);
	}
	
	@Test
	public void calculateSignalStrength_inputInstructions() throws IOException {
		SequencedCollection<String> inputHill = InputReader.readFile("day12.txt");
		HillClimbingAlgorithm hillClimbing = new HillClimbingAlgorithm(inputHill);
		assertEquals(408, hillClimbing.getShortestPath().size()-1);
	}
	
	@Test
	public void findShortestHillHikingTrail_simpleInstructions() {
		HillClimbingAlgorithm hillClimbing = new HillClimbingAlgorithm(simpleHill);
		assertEquals(29, hillClimbing.getShortestPathToEdge().size()-1);
	}
	
	@Test
	public void findShortestHillHikingTrail_inputInstructions() throws IOException {
		SequencedCollection<String> inputHill = InputReader.readFile("day12.txt");
		HillClimbingAlgorithm hillClimbing = new HillClimbingAlgorithm(inputHill);
		assertEquals(399, hillClimbing.getShortestPathToEdge().size()-1);
	}
}
