package fr.grunberg.adventofcode2022.day13;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;
import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class DistressSignalAnalyzerTest {
	private final SequencedCollection<String> simpleDistressSignal = List.of("""
			[1,1,3,1,1]
			[1,1,5,1,1]
			
			[[1],[2,3,4]]
			[[1],4]
			
			[9]
			[[8,7,6]]
			
			[[4,4],4,4]
			[[4,4],4,4,4]
			
			[7,7,7,7]
			[7,7,7]
			
			[]
			[3]
			
			[[[]]]
			[[]]
			
			[1,[2,[3,[4,[5,6,7]]]],8,9]
			[1,[2,[3,[4,[5,6,0]]]],8,9]"""
			.split("\n")
			);

	@Test
	public void calculateCorrectIndices_simpleInstructions() {
		DistressSignalAnalyzer analyzer = new DistressSignalAnalyzer(simpleDistressSignal);
		assertEquals(13, analyzer.getCorrectIndicesSum());
	}
	
	@Test
	public void calculateCorrectIndices_inputInstructions() throws IOException {
		SequencedCollection<String> inputDistressSignal = InputReader.readFile("day13.txt");
		DistressSignalAnalyzer analyzer = new DistressSignalAnalyzer(inputDistressSignal);
		assertEquals(4894, analyzer.getCorrectIndicesSum());
	}
	
	@Test
	public void calculateDividerIndices_simpleInstructions() {
		DistressSignalAnalyzer analyzer = new DistressSignalAnalyzer(simpleDistressSignal);
		assertEquals(140, analyzer.addDividerPacketsSortAndLocate());
	}
	
	@Test
	public void calculateDividerIndices_inputInstructions() throws IOException {
		SequencedCollection<String> inputDistressSignal = InputReader.readFile("day13.txt");
		DistressSignalAnalyzer analyzer = new DistressSignalAnalyzer(inputDistressSignal);
		assertEquals(24180, analyzer.addDividerPacketsSortAndLocate());
	}
	
	@Test
	public void parserTest_SimpleList() {
		PacketData packets = DistressSignalAnalyzer.parse("[1,1,3,1,1]");
		assertTrue(packets instanceof PacketDataList);
		assertEquals(5, packets.asList().size());
	}
	
	@Test
	public void parserTest_EmbeddedList() {
		PacketData packets = DistressSignalAnalyzer.parse("[[8,7,6]]");
		assertTrue(packets instanceof PacketDataList);
		assertEquals(1, packets.asList().size());
		assertEquals(3, packets.asList().getFirst().asList().size());
	}
	
	@Test
	public void parserTest_EmbeddedLists() {
		PacketData packets = DistressSignalAnalyzer.parse("[[1],[2,3,4]]");
		assertTrue(packets instanceof PacketDataList);
		assertEquals(2, packets.asList().size());
		assertEquals(1, packets.asList().get(0).asList().size());
		assertEquals(3, packets.asList().get(1).asList().size());
	}
	
	@Test
	public void parserTest_EmbeddedListsWithSingleIntegers() {
		PacketData packets = DistressSignalAnalyzer.parse("[1,[5,6,7],8,9]");
		assertTrue(packets instanceof PacketDataList);
		assertEquals(4, packets.asList().size());
		assertEquals(3, packets.asList().get(1).asList().size());
	}

	@Test
	public void parserTest_EmptyList() {
		PacketData packets = DistressSignalAnalyzer.parse("[]");
		assertTrue(packets instanceof PacketDataList);
		assertEquals(0, packets.asList().size());
	}

	@Test
	public void parserTest_EmbeddedEmptyList() {
		PacketData packets = DistressSignalAnalyzer.parse("[[]]");
		assertTrue(packets instanceof PacketDataList);
		assertEquals(1, packets.asList().size());
		assertEquals(0, packets.asList().getFirst().asList().size());
	}
}
