package fr.grunberg.adventofcode2022.day2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;

import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class ShiFuMiCalculatorTest {
	private final List<String> simpleStrategy = List.of(
			"A Y",
			"B X",
			"C Z"
			);
	
	@Test
	public void getScore_SimpleTest() {
		assertEquals(15, new ShiFuMiCalculator(simpleStrategy).getBestScore());
	}
	
	@Test
	public void getScore_InputTest() throws IOException {
		SequencedCollection<String> inputTest = InputReader.readFile("day2.txt");
		assertEquals(14531, new ShiFuMiCalculator(inputTest).getBestScore());
	}
	
	@Test
	public void getScoreLessSuspicious_SimpleTest() {
		assertEquals(12, new ShiFuMiCalculator(simpleStrategy).getLessSuspiciousScore());
	}
	
	@Test
	public void getScoreLessSuspicious_InputTest() throws IOException {
		SequencedCollection<String> inputTest = InputReader.readFile("day2.txt");
		assertEquals(11258, new ShiFuMiCalculator(inputTest).getLessSuspiciousScore());
	}
}
