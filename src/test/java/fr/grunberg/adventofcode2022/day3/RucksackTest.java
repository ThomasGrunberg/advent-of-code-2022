package fr.grunberg.adventofcode2022.day3;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.SequencedCollection;

import org.apache.commons.collections4.ListUtils;
import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class RucksackTest {
	private final Collection<String> simpleSort = List.of(
		"vJrwpWtwJgWrhcsFMMfFFhFp",
		"jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
		"PmmdzqPrVvPwwTWBwg",
		"wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
		"ttgJtRGJQctTZtZT",
		"CrZsJsPPZsGzwwsLwLmpwMDw"
		);
	
	@Test
	public void getWronglySortedItems_SimpleSort() {
		int total = simpleSort.stream()
				.map(l -> new Rucksack(l))
				.map(Rucksack::getWronglySortedItem)
				.mapToInt(Item::getPriority)
				.sum();
		assertEquals(157, total);
	}
	
	@Test
	public void getWronglySortedItems_InputSort() throws IOException {
		SequencedCollection<String> inputTest = InputReader.readFile("day3.txt");
		int total = inputTest.stream()
				.map(l -> new Rucksack(l))
				.map(Rucksack::getWronglySortedItem)
				.mapToInt(Item::getPriority)
				.sum();
		assertEquals(7795, total);
	}
	
	@Test
	public void getBadges_SimpleSort() {
		int badgeTotals = 
			ListUtils.partition(simpleSort.stream().map(l -> new Rucksack(l)).toList(), 3)
				.parallelStream()
				.map(g -> Rucksack.getBadge(g))
				.mapToInt(i -> i.getPriority())
				.sum();
		assertEquals(70, badgeTotals);
	}
	
	@Test
	public void getBadges_InputSort() throws IOException {
		SequencedCollection<String> inputTest = InputReader.readFile("day3.txt");
		int badgeTotals = 
			ListUtils.partition(inputTest.stream().map(l -> new Rucksack(l)).toList(), 3)
				.parallelStream()
				.map(g -> Rucksack.getBadge(g))
				.mapToInt(i -> i.getPriority())
				.sum();
		assertEquals(2703, badgeTotals);
	}
}
