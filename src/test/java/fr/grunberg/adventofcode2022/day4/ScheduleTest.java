package fr.grunberg.adventofcode2022.day4;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.SequencedCollection;

import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class ScheduleTest {
	private final Collection<String> simpleSchedule = List.of(
		"2-4,6-8",
		"2-3,4-5",
		"5-7,7-9",
		"2-8,3-7",
		"6-6,4-6",
		"2-6,4-8"
		);
	
	@Test
	public void scheduleCompleteOverlap_SimpleSchedule() {
		assertEquals(2, simpleSchedule.stream()
				.map(l -> new TaskPair(l))
				.mapToInt(TaskPair::getTaskCompleteOverlap)
				.sum());
	}
	
	@Test
	public void scheduleCompleteOverlap_InputSchedule() throws IOException {
		SequencedCollection<String> inputSchedule = InputReader.readFile("day4.txt");
		assertEquals(498, inputSchedule.stream()
				.map(l -> new TaskPair(l))
				.mapToInt(TaskPair::getTaskCompleteOverlap)
				.sum());
	}
	
	@Test
	public void schedulePartialOverlap_SimpleSchedule() {
		assertEquals(4, simpleSchedule.stream()
				.map(l -> new TaskPair(l))
				.mapToInt(TaskPair::getTaskPartialOverlap)
				.sum());
	}
	
	@Test
	public void schedulePartialOverlap_InputSchedule() throws IOException {
		SequencedCollection<String> inputSchedule = InputReader.readFile("day4.txt");
		assertEquals(859, inputSchedule.stream()
				.map(l -> new TaskPair(l))
				.mapToInt(TaskPair::getTaskPartialOverlap)
				.sum());
	}
}
