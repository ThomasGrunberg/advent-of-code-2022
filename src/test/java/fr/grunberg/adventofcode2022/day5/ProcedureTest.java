package fr.grunberg.adventofcode2022.day5;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;

import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class ProcedureTest {
	private static final SequencedCollection<String> simpleProcedure = List.of(
		    "    [D]    ",
    		"[N] [C]    ",
    		"[Z] [M] [P]",
    		" 1   2   3 ",
    		"",
    		"move 1 from 2 to 1",
    		"move 3 from 1 to 3",
    		"move 2 from 2 to 1",
    		"move 1 from 1 to 2"
			);
	
	@Test
	public void runProcedure_simple() {
		assertEquals("CMZ", new Procedure(simpleProcedure, false).getAvailableCrates());
	}
	
	@Test
	public void runProcedure_input() throws IOException {
		SequencedCollection<String> inputSchedule = InputReader.readFile("day5.txt");
		assertEquals("TDCHVHJTG", new Procedure(inputSchedule, false).getAvailableCrates());
	}
	
	@Test
	public void runProcedure_simpleMultiMove() {
		assertEquals("MCD", new Procedure(simpleProcedure, true).getAvailableCrates());
	}
	
	@Test
	public void runProcedure_inputMultiMove() throws IOException {
		SequencedCollection<String> inputSchedule = InputReader.readFile("day5.txt");
		assertEquals("NGCMPJLHV", new Procedure(inputSchedule, true).getAvailableCrates());
	}
}
