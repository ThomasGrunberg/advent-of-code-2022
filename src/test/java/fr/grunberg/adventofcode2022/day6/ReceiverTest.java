package fr.grunberg.adventofcode2022.day6;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;

import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class ReceiverTest {
	private final List<String> messages = List.of(
			"mjqjpqmgbljsphdztnvjfqwrcgsmlb",
			"bvwbjplbgvbhsrlpgdmjqwftvncz",
			"nppdvjthqldpwncqszvftbrmjlhg",
			"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg",
			"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"
			);
	
	@Test
	public void findStartOfPacketMarker_simpleMessage() {
		Receiver receiver = new Receiver();
		assertEquals(7,  receiver.findStartOfPacketMarker(messages.get(0)));
		assertEquals(5,  receiver.findStartOfPacketMarker(messages.get(1)));
		assertEquals(6,  receiver.findStartOfPacketMarker(messages.get(2)));
		assertEquals(10, receiver.findStartOfPacketMarker(messages.get(3)));
		assertEquals(11, receiver.findStartOfPacketMarker(messages.get(4)));
	}
	
	@Test
	public void findStartOfPacketMarker_inputMessage() throws IOException {
		SequencedCollection<String> inputSchedule = InputReader.readFile("day6.txt");
		Receiver receiver = new Receiver();
		assertEquals(1892,  receiver.findStartOfPacketMarker(inputSchedule.getFirst()));
	}
	
	@Test
	public void findStartOfMessageMarker_simpleMessage() {
		Receiver receiver = new Receiver();
		assertEquals(19,  receiver.findStartOfMessageMarker(messages.get(0)));
		assertEquals(23,  receiver.findStartOfMessageMarker(messages.get(1)));
		assertEquals(23,  receiver.findStartOfMessageMarker(messages.get(2)));
		assertEquals(29, receiver.findStartOfMessageMarker(messages.get(3)));
		assertEquals(26, receiver.findStartOfMessageMarker(messages.get(4)));
	}
	
	@Test
	public void findStartOfMessageMarker_inputMessage() throws IOException {
		SequencedCollection<String> inputSchedule = InputReader.readFile("day6.txt");
		Receiver receiver = new Receiver();
		assertEquals(2313,  receiver.findStartOfMessageMarker(inputSchedule.getFirst()));
	}
}
