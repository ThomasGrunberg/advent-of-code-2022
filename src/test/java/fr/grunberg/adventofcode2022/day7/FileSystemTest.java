package fr.grunberg.adventofcode2022.day7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;

import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class FileSystemTest {
	private final SequencedCollection<String> fsExploration = List.of(
			"$ cd /",
			"$ ls",
			"dir a",
			"14848514 b.txt",
			"8504156 c.dat",
			"dir d",
			"$ cd a",
			"$ ls",
			"dir e",
			"29116 f",
			"2557 g",
			"62596 h.lst",
			"$ cd e",
			"$ ls",
			"584 i",
			"$ cd ..",
			"$ cd ..",
			"$ cd d",
			"$ ls",
			"4060174 j",
			"8033020 d.log",
			"5626152 d.ext",
			"7214296 k"
			);
	
	@Test
	public void getSmallDirectories_simpleFS() {
		FileSystem fileSystem = new FileSystem(fsExploration, 70_000_000);
		assertEquals(95437, fileSystem.getSmallDirectoriesSize());
	}
	
	@Test
	public void getSmallDirectories_inputFS() throws IOException {
		SequencedCollection<String> inputFS = InputReader.readFile("day7.txt");
		FileSystem fileSystem = new FileSystem(inputFS, 70_000_000);
		assertEquals(1648397, fileSystem.getSmallDirectoriesSize());
	}
	
	@Test
	public void getDirectoryToRemove_simpleFS() {
		FileSystem fileSystem = new FileSystem(fsExploration, 70_000_000);
		assertEquals(24_933_642, fileSystem.getDirectoryToRemove(30_000_000).getSize());
	}
	
	@Test
	public void getDirectoryToRemove_inputFS() throws IOException {
		SequencedCollection<String> inputFS = InputReader.readFile("day7.txt");
		FileSystem fileSystem = new FileSystem(inputFS, 70_000_000);
		assertEquals(1_815_525, fileSystem.getDirectoryToRemove(30_000_000).getSize());
	}
}
