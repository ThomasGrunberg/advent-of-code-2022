package fr.grunberg.adventofcode2022.day8;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;

import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class ForestTest {
	private final SequencedCollection<String> simpleForest = List.of(
			"30373",
			"25512",
			"65332",
			"33549",
			"35390"
			);
	
	@Test
	public void countVisibleTrees_simpleForest() {
		Forest forest = new Forest(simpleForest);
		assertEquals(21, forest.countVisibleTrees());
	}
	
	@Test
	public void countVisibleTrees_inputForest() throws IOException {
		SequencedCollection<String> inputForest = InputReader.readFile("day8.txt");
		Forest forest = new Forest(inputForest);
		assertEquals(1854, forest.countVisibleTrees());
	}
	
	@Test
	public void getHighestScenicScore_simpleForest() {
		Forest forest = new Forest(simpleForest);
		assertEquals(8, forest.getHighestScenicScore());
	}
	
	@Test
	public void getHighestScenicScore_inputForest() throws IOException {
		SequencedCollection<String> inputForest = InputReader.readFile("day8.txt");
		Forest forest = new Forest(inputForest);
		assertEquals(527340, forest.getHighestScenicScore());
	}
}
