package fr.grunberg.adventofcode2022.day9;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.SequencedCollection;

import org.junit.jupiter.api.Test;

import fr.grunberg.adventofcode2022.InputReader;

public class RopeBridgeTest {
	private final SequencedCollection<String> simpleRopeMovements = List.of(
			"R 4",
			"U 4",
			"L 3",
			"D 1",
			"R 4",
			"D 1",
			"L 5",
			"R 2"
			);

	private final SequencedCollection<String> simpleLargeRopeMovements = List.of(
			"R 5",
			"U 8",
			"L 8",
			"D 3",
			"R 17",
			"D 10",
			"L 25",
			"U 20"
			);
	
	@Test
	public void countRopeTailPositions_simpleStretch() {
		RopeBridge bridge = new RopeBridge(simpleRopeMovements, 2);
		assertEquals(13, bridge.countAllPositions());
	}
	
	@Test
	public void countRopeTailPositions_inputStretch() throws IOException {
		SequencedCollection<String> inputRopeMovements = InputReader.readFile("day9.txt");
		RopeBridge bridge = new RopeBridge(inputRopeMovements, 2);
		assertEquals(6266, bridge.countAllPositions());
	}
	
	@Test
	public void countLongRopeTailPositions_simpleLargeStretch() {
		RopeBridge bridge = new RopeBridge(simpleLargeRopeMovements, 10);
		assertEquals(36, bridge.countAllPositions());
	}

	@Test
	public void countLongRopeTailPositions_inputStretch() throws IOException {
		SequencedCollection<String> inputRopeMovements = InputReader.readFile("day9.txt");
		RopeBridge bridge = new RopeBridge(inputRopeMovements, 10);
		assertEquals(2369, bridge.countAllPositions());
	}
}
